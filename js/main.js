class GameItem {
    constructor(name, xPosition = 0, yPosition = 0) {
        this._name = name;
        this._xPos = xPosition;
        this._yPos = yPosition;
    }
    set xPos(xPosition) {
        this._xPos = xPosition;
    }
    set yPos(yPosition) {
        this._yPos = yPosition;
    }
    draw(container) {
        this._element = document.createElement('div');
        this._element.className = this._name;
        this._element.id = this._name;
        this._element.style.transform = `translate(${this._xPos}px, ${this._yPos}px)`;
        const image = document.createElement('img');
        image.src = `./assets/img/${this._name}.png `;
        this._element.appendChild(image);
        container.appendChild(this._element);
    }
    update() {
        this._element.style.transform = `translate(${this._xPos}px, ${this._yPos}px)`;
    }
}
class Car extends GameItem {
    constructor(name, xPosition = 0, yPosition = 0) {
        super(name, xPosition, yPosition);
    }
    move(xPosition) {
        this._xPos -= xPosition;
        this._element.classList.add('driving');
        console.log("Car moved to xPosition: " + this.xPosition + ".");
    }
    get xPosition() {
        return this._xPos;
    }
}
class Game {
    constructor() {
        this._element = document.getElementById('container');
        this.keyDownHandler = (e) => {
            if (e.keyCode === 87) {
                this._car.move(25);
                this.update();
            }
            if (e.keyCode === 38) {
                this._car2.move(25);
                this.update();
            }
        };
        this._car2 = new Car('car2');
        this._car = new Car('car');
        this._xspot = new Xspot('xspot', 0);
        this._scoreboard = new Scoreboard('scoreboard');
        this._scoreboard2 = new Scoreboard('scoreboard2');
        window.addEventListener('keydown', this.keyDownHandler);
        this.draw();
    }
    collision() {
        const xspotRect = document.getElementById('xspot-0').getBoundingClientRect();
        const carRect = document.getElementById('car').getBoundingClientRect();
        const car2Rect = document.getElementById('car2').getBoundingClientRect();
        if (xspotRect.right >= carRect.right) {
            this._car.move(this._car.xPosition);
            this._scoreboard.addScore();
        }
        if (xspotRect.right >= car2Rect.right) {
            this._car2.move(this._car2.xPosition);
            this._scoreboard2.addScore();
        }
        else {
            console.log('no collision');
        }
    }
    draw() {
        this._car.draw(this._element);
        this._car2.draw(this._element);
        this._xspot.draw(this._element);
        this._scoreboard.draw(this._element);
        this._scoreboard2.draw(this._element);
    }
    update() {
        this.collision();
        this._car.update();
        this._car2.update();
        this._xspot.update();
        this._scoreboard.update();
        this._scoreboard2.update();
    }
}
let app;
(function () {
    let init = function () {
        app = new Game();
    };
    window.addEventListener('load', init);
})();
class Scoreboard extends GameItem {
    constructor(name) {
        super(name);
        this._score = 0;
        this._score2 = 0;
    }
    get score() {
        return this._score;
    }
    draw(container) {
        this._element = document.createElement('div');
        this._element.className = this._name;
        this._element.id = this._name;
        const p = document.createElement('p');
        p.innerHTML = 'The score is: ';
        const span = document.createElement('span');
        span.innerHTML = this._score.toString();
        p.appendChild(span);
        this._element.appendChild(p);
        container.appendChild(this._element);
    }
    update() {
        const scoreSpan = this._element.childNodes[0].childNodes[1];
        scoreSpan.innerHTML = this._score.toString();
    }
    addScore() {
        this._score += 10;
        if (this._score == 50) {
            alert("Player 1 wins");
        }
        if (this._score2 == 50) {
            alert("Player 2 wins");
        }
    }
}
class Xspot extends GameItem {
    constructor(name, id, xPosition = 0, yPosition = 0) {
        super(name, xPosition, yPosition);
        this._id = id;
    }
    draw(container) {
        this._element = document.createElement('div');
        this._element.className = this._name;
        this._element.id = `${this._name}-${this._id}`;
        this._element.style.transform = `translate(${this._xPos}px, ${this._yPos}px)`;
        const image = document.createElement('img');
        image.src = `./assets/img/${this._name}.png `;
        this._element.appendChild(image);
        container.appendChild(this._element);
    }
    remove(container) {
        const elem = document.getElementById(`${this._name}-${this._id}`);
        container.removeChild(elem);
    }
}
//# sourceMappingURL=main.js.map