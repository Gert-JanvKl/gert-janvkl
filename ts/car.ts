/// <reference path="gameItem.ts" />

class Car extends GameItem {

    /**
    * Function to create the car
    * @param {string} - name
    * @param {number} - xPosition
    * @param {number} - yPosition
    */
    constructor(name: string, xPosition: number = 0, yPosition: number = 0) {
        super(name, xPosition, yPosition);
    }

    /**
    * Function to move the car to the left
    * @param {number} - yPosition
    */
    public move(xPosition: number): void {
        this._xPos -= xPosition;
        this._element.classList.add('driving');
        console.log("Car moved to xPosition: " + this.xPosition + ".");
    }
    /**
    * Returns the x-Position of the car
    */
    public get xPosition(): number {
        return this._xPos;
    }
}