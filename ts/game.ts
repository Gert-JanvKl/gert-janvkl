class Game {
  //attr
  private _element: HTMLElement = document.getElementById('container');
  private _car: Car;
  private _car2: Car;
  private _xspot: Xspot; //use an array if you have multiple gameItems of the same sort
  private _scoreboard: Scoreboard;
  private _scoreboard2: Scoreboard;

  /**
   * Create the Game class
   */
  constructor() {
    //create some gameItems
    this._car2 = new Car('car2')
    this._car = new Car('car');
    this._xspot = new Xspot('xspot', 0);
    this._scoreboard = new Scoreboard('scoreboard');
    this._scoreboard2 = new Scoreboard('scoreboard2')

    //add keydown handler to the window object
    window.addEventListener('keydown', this.keyDownHandler);

    //draw is initial state
    this.draw();
  }

  /**
   * Function to detect collision between two objects
   */
  public collision(): void {
    //use elem.getBoundingClientRect() for getting the wright coordinates and measurements of the element
    const xspotRect = document.getElementById('xspot-0').getBoundingClientRect();
    const carRect = document.getElementById('car').getBoundingClientRect();
    const car2Rect = document.getElementById('car2').getBoundingClientRect();
    if (xspotRect.right >= carRect.right) {
      this._car.move(this._car.xPosition);
      this._scoreboard.addScore();
    }
    if (xspotRect.right >= car2Rect.right) {
      this._car2.move(this._car2.xPosition);
      this._scoreboard2.addScore();
    } 
    else {
      console.log('no collision');
    }
  }

  /**
   * Function to draw the initial state of al living objects
   */
  public draw(): void {
    this._car.draw(this._element);
    this._car2.draw(this._element);
    this._xspot.draw(this._element);
    this._scoreboard.draw(this._element);
    this._scoreboard2.draw(this._element);
  }

  /**
   * Function to update the state of all living objects
   */
  public update(): void { //function formerly known as render()
    this.collision();
    this._car.update();
    this._car2.update();
    this._xspot.update();
    this._scoreboard.update();
    this._scoreboard2.update();
  }

  /**
   * Function to handle the keyboard event
   * @param {KeyboardEvent} - event
   */
  public keyDownHandler = (e: KeyboardEvent): void => {
    if (e.keyCode === 87) {
      
      //move car 25px
      this._car.move(25);
      this.update();
    }
    if (e.keyCode === 38) {

      //move car 2 25 px
      this._car2.move(25);
      this.update();
    }
  }
}